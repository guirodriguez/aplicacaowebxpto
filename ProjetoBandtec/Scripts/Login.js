﻿$(document).ready(function () {
    $("#status").hide();
    $(".btn").click(function () {
        $.ajax({
            url: "/Usuarios/AutenticacaoDeUsuario",
            data: {
                Login: $(".nome_usuario").val(),
                Senha: $(".senha").val()
            },
            dataType: "json",
            type: "GET",
            async: true,
            beforeSend: function () {
                $("#status").html("Estamos autenticando o usuário. Só um instante...");
                $("#status").show();
            },
            success: function (dados) {
                if (dados.OK) {
                    $("#status").html(dados.Mensagem)
                    setTimeout(function () { window.location.href = "/Home/Inicial" }, 5000);
                    $("#status").show();
                } else {
                    $("#status").html(dados.Mensagem);
                    $("#status").show();
                }
            },
            error: function () {
                $("#status").html(dados.Mensagem);
                $("#status").show()
            }
        });
    });
});