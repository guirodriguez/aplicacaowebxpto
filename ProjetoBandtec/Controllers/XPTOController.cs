﻿using ProjetoBandtec.Models;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using System.Web;

namespace ProjetoBandtec.Controllers
{
    public class XPTOController : Controller
    {

        [HttpGet]
        public ActionResult Login()
        {
            if (Session["usuarioLogadoID"] != null)
            {
                return RedirectToAction("Home");
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(xptotba02_analista usuario)
        {
            if (ModelState.IsValid)
            {
                using (ModeloDeDados bd = new ModeloDeDados())
                {
                    //var clienteanalista = Mapper.Map<xptotba02_analista, ClienteAnalistaViewModel>(usuario);
                    var v = bd.xptotba02_analista.Where(a => a.nome_usuario.Equals(usuario.nome_usuario) && a.senha.Equals(usuario.senha)).FirstOrDefault();

                    if (v != null)
                    {
                        HttpCookie cookieLogin = new HttpCookie("login", v.cliente_hostname);
                        cookieLogin.Expires = DateTime.Now.AddYears(1);
                        Response.Cookies.Add(cookieLogin);
                        Session["usuarioLogadoID"] = v.senha.ToString();
                        Session["nomeUsuarioLogado"] = v.nome_usuario.ToString();
                        return RedirectToAction("Home");
                    }
                    else
                    {
                        ViewBag.Alerta = "Usuário não é válido, por favor digite novamente ou se cadastre";
                    }
                }
            }
            return View(usuario);
        }

        [HttpGet]
        public ActionResult Cadastro()
        {

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Cadastro(Cadastro cadastro)
        {
            if (ModelState.IsValid)
            {
                xptotba01_cliente cliente = new xptotba01_cliente();
                ModeloDeDados bd = new ModeloDeDados();

                if (!ClienteDAL.VerificaUsuario(cadastro.Usuario))
                {
                    cliente.hostname = cadastro.Hostname;
                    cliente.dominio_email = cadastro.DominioEmail;
                    cliente.cnpj = cadastro.Cnpj;
                    cliente.razao_social = cadastro.RazaoSocial;
                    cliente.codigo = "";
                    bd.xptotba01_cliente.Add(cliente);
                    xptotba02_analista analista = new xptotba02_analista();
                    analista.nome_usuario = cadastro.Usuario;
                    analista.senha = cadastro.Senha;
                    analista.xptotba01_cliente = cliente;
                    bd.xptotba02_analista.Add(analista);
                    bd.SaveChanges();
                    ModelState.Clear();
                    cadastro = null;
                    ViewBag.Message = "Usuário registrado com sucesso.";
                }
                else
                {
                    ViewBag.Message = "Usuário já cadastrado.";
                }

            }
            return View(cadastro);
        }


        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            return RedirectToAction("Login", "XPTO");
        }

        [HttpGet]
        public ActionResult Maquina(String serial)
        {
            HttpCookie cookieLogin = new HttpCookie("serial", serial);
            cookieLogin.Expires = DateTime.Now.AddYears(1);
            Response.Cookies.Add(cookieLogin);
            if (Session["usuarioLogadoID"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login");
            }
        }


        public ActionResult Home()
        {
            if (Session["usuarioLogadoID"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login");
            }
        }

        public ActionResult Download()
        {
            if (Session["usuarioLogadoID"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login");
            }
        }

        public ActionResult Sobre()
        {
            return View();
        }
    }
}