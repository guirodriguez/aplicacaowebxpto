﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using ProjetoBandtec.Models;

namespace ProjetoBandtec.Controllers
{
    public class CRUDController : Controller
    {
        private ModeloDeDados db = new ModeloDeDados();

        // GET: CRUD
        public ActionResult Index()
        {
            var xptotba02_analista = db.xptotba02_analista.Include(x => x.xptotba01_cliente);
            return View(xptotba02_analista.ToList());
        }

        // GET: CRUD/Details/5
        public ActionResult Detalhes(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            xptotba02_analista xptotba02_analista = db.xptotba02_analista.Find(id);
            if (xptotba02_analista == null)
            {
                return HttpNotFound();
            }
            return View(xptotba02_analista);
        }

        // GET: CRUD/Create
        public ActionResult AdicionarAnalista()
        {
            ViewBag.cliente_hostname = new SelectList(db.xptotba01_cliente, "hostname", "cnpj");
            return View();
        }

        // POST: CRUD/Create
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AdicionarAnalista([Bind(Include = "id,nome_usuario,senha,tipo_usuario,cliente_hostname")] xptotba02_analista xptotba02_analista)
        {
            if (ModelState.IsValid)
            {
                db.xptotba02_analista.Add(xptotba02_analista);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.cliente_hostname = new SelectList(db.xptotba01_cliente, "hostname", "cnpj", xptotba02_analista.cliente_hostname);
            return View(xptotba02_analista);
        }

        // GET: CRUD/Edit/5
        public ActionResult EditarAnalista(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            xptotba02_analista xptotba02_analista = db.xptotba02_analista.Find(id);
            if (xptotba02_analista == null)
            {
                return HttpNotFound();
            }
            ViewBag.cliente_hostname = new SelectList(db.xptotba01_cliente, "hostname", "cnpj", xptotba02_analista.cliente_hostname);
            return View(xptotba02_analista);
        }

        // POST: CRUD/Edit/5
        // Para se proteger de mais ataques, ative as propriedades específicas a que você quer se conectar. Para 
        // obter mais detalhes, consulte https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditarAnalista([Bind(Include = "id,nome_usuario,senha,tipo_usuario,cliente_hostname")] xptotba02_analista xptotba02_analista)
        {
            if (ModelState.IsValid)
            {
                db.Entry(xptotba02_analista).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.cliente_hostname = new SelectList(db.xptotba01_cliente, "hostname", "cnpj", xptotba02_analista.cliente_hostname);
            return View(xptotba02_analista);
        }

        // GET: CRUD/Delete/5
        public ActionResult DeletarAnalista(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            xptotba02_analista xptotba02_analista = db.xptotba02_analista.Find(id);
            if (xptotba02_analista == null)
            {
                return HttpNotFound();
            }
            return View(xptotba02_analista);
        }

        // POST: CRUD/Delete/5
        [HttpPost, ActionName("DeletarAnalista")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            xptotba02_analista xptotba02_analista = db.xptotba02_analista.Find(id);
            db.xptotba02_analista.Remove(xptotba02_analista);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
