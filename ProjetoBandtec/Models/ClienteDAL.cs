﻿using System.Linq;

namespace ProjetoBandtec.Models
{
    public class ClienteDAL
    {
        public static bool VerificaUsuario(string usuario)
        {
            using (ModeloDeDados bd = new ModeloDeDados())
            {
                var existeUsuario = (from u in bd.xptotba02_analista
                                   where u.nome_usuario == usuario
                                   select u).FirstOrDefault();
                if (existeUsuario != null)
                {
                    return true;
                }

                else
                {
                    return false;
                }
                    
            }
        }
    }
}