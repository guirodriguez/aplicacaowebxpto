﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ProjetoBandtec.Models
{
    public class Cadastro
    {
        [Required(ErrorMessage = "O campo Hostname é obrigatório")]
        public String Hostname { get; set; }

        [Required(ErrorMessage = "O campo CNPJ é obrigatório")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:##############}")]
        [StringLength(14, MinimumLength = 14, ErrorMessage = "Digite um CNPJ válido")]
        public String Cnpj { get; set; }

        [Required(ErrorMessage = "O campo Dominio de Email é obrigatório")]
        public String DominioEmail { get; set; }

        [Required(ErrorMessage = "O campo Nome da Empresa é obrigatório")]
        public String RazaoSocial { get; set; }

        [Required(ErrorMessage = "O campo Seu Usuário é obrigatório", AllowEmptyStrings = false)]
        public String Usuario { get; set; }

        [Required(ErrorMessage = "O campo Sua Senha é obrigatório", AllowEmptyStrings = false)]
        [DataType(System.ComponentModel.DataAnnotations.DataType.Password)]
        [RegularExpression(@"^.*(?=.{8,})(?=.*[@#$%^&+=])(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*$", ErrorMessage = "Senha deve ter no minimo 8 caracteres, uma letra maiúscula, uma letra minuscula, um digito(0..9) e um carácter especial")]
        public String Senha { get; set; }
    }
}