namespace ProjetoBandtec.Models
{
    using System.ComponentModel.DataAnnotations;

    public partial class xptotba06_hardware
    {
        public int id { get; set; }

        [StringLength(255)]
        public string arquiteura_sistema_operacional { get; set; }

        [StringLength(255)]
        public string codigo_processador { get; set; }

        [StringLength(255)]
        public string data_fabricacao { get; set; }

        [StringLength(255)]
        public string descricao_fabricante { get; set; }

        [StringLength(255)]
        public string descricao_processador { get; set; }

        [StringLength(255)]
        public string descricao_sistema_operacional { get; set; }

        [StringLength(255)]
        public string espaco_total_ram { get; set; }

        [StringLength(255)]
        public string identificador_processador { get; set; }

        [StringLength(255)]
        public string modelo_especifico_fabricante { get; set; }

        [StringLength(255)]
        public string modelo_fabricante { get; set; }

        [StringLength(255)]
        public string quantidade_fisica_processador { get; set; }

        [StringLength(255)]
        public string quantidade_logica_processador { get; set; }

        [StringLength(255)]
        public string usuario_logado { get; set; }

        [StringLength(255)]
        public string maquina_serial { get; set; }

        public virtual xptotba03_maquina xptotba03_maquina { get; set; }
    }
}
