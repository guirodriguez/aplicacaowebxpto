namespace ProjetoBandtec.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class xptotba03_maquina
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public xptotba03_maquina()
        {
            xptotba06_hardware = new HashSet<xptotba06_hardware>();
            xptotba05_hd = new HashSet<xptotba05_hd>();
            xptotba04_usuariomaquina = new HashSet<xptotba04_usuariomaquina>();
            xptotba07_monitoramento = new HashSet<xptotba07_monitoramento>();
        }

        [Key]
        [StringLength(255)]
        public string serial { get; set; }

        [StringLength(255)]
        public string cliente_hostname { get; set; }

        public virtual xptotba01_cliente xptotba01_cliente { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<xptotba06_hardware> xptotba06_hardware { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<xptotba05_hd> xptotba05_hd { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<xptotba04_usuariomaquina> xptotba04_usuariomaquina { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<xptotba07_monitoramento> xptotba07_monitoramento { get; set; }
    }
}
