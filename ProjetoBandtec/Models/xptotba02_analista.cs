namespace ProjetoBandtec.Models
{
    using System.ComponentModel.DataAnnotations;

    public partial class xptotba02_analista
    {
        public int id { get; set; }

        [Required(ErrorMessage = "Informe o nome do usu�rio", AllowEmptyStrings = false)]
        public string nome_usuario { get; set; }

        [Required(ErrorMessage = "Informe a senha do usu�rio", AllowEmptyStrings = false)]
        public string senha { get; set; }

        public int? tipo_usuario { get; set; }

        [StringLength(255)]
        public string cliente_hostname { get; set; }

        public virtual xptotba01_cliente xptotba01_cliente { get; set; }
    }
}
