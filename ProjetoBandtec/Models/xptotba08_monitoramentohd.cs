namespace ProjetoBandtec.Models
{
    using System.ComponentModel.DataAnnotations;

    public partial class xptotba08_monitoramentohd
    {
        public int id { get; set; }

        public double? percentual_utilizado { get; set; }

        public int? monitoramento_id { get; set; }

        [StringLength(255)]
        public string caminho_absoluto { get; set; }

        public virtual xptotba07_monitoramento xptotba07_monitoramento { get; set; }
    }
}
