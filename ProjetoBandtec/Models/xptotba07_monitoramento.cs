namespace ProjetoBandtec.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class xptotba07_monitoramento
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public xptotba07_monitoramento()
        {
            xptotba08_monitoramentohd = new HashSet<xptotba08_monitoramentohd>();
        }

        public int id { get; set; }

        public double? consumo_cpu { get; set; }

        public double? percentual_ram { get; set; }

        public double? temperatura_cpu { get; set; }

        [StringLength(255)]
        public string maquina_serial { get; set; }

        public virtual xptotba03_maquina xptotba03_maquina { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<xptotba08_monitoramentohd> xptotba08_monitoramentohd { get; set; }
    }
}
