namespace ProjetoBandtec.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class xptotba01_cliente
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public xptotba01_cliente()
        {
            xptotba02_analista = new HashSet<xptotba02_analista>();
            xptotba03_maquina = new HashSet<xptotba03_maquina>();
        }

        [Key]
        [StringLength(255)]
        public string hostname { get; set; }

        public string cnpj { get; set; }

        [StringLength(255)]
        public string dominio_email { get; set; }

        [StringLength(255)]
        public string razao_social { get; set; }

        [StringLength(255)]
        public string codigo { get; set; }

        [StringLength(255)]
        public string canal { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<xptotba02_analista> xptotba02_analista { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<xptotba03_maquina> xptotba03_maquina { get; set; }
    }
}
