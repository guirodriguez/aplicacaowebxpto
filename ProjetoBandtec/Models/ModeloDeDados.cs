namespace ProjetoBandtec.Models
{
    using System.Data.Entity;

    public partial class ModeloDeDados : DbContext
    {
        public ModeloDeDados()
            : base("name=ModeloDeDados")
        {
        }

        public virtual DbSet<xptotba01_cliente> xptotba01_cliente { get; set; }
        public virtual DbSet<xptotba02_analista> xptotba02_analista { get; set; }
        public virtual DbSet<xptotba03_maquina> xptotba03_maquina { get; set; }
        public virtual DbSet<xptotba04_usuariomaquina> xptotba04_usuariomaquina { get; set; }
        public virtual DbSet<xptotba05_hd> xptotba05_hd { get; set; }
        public virtual DbSet<xptotba06_hardware> xptotba06_hardware { get; set; }
        public virtual DbSet<xptotba07_monitoramento> xptotba07_monitoramento { get; set; }
        public virtual DbSet<xptotba08_monitoramentohd> xptotba08_monitoramentohd { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<xptotba01_cliente>()
                .Property(e => e.hostname)
                .IsUnicode(false);

            modelBuilder.Entity<xptotba01_cliente>()
                .Property(e => e.cnpj)
                .IsUnicode(false);

            modelBuilder.Entity<xptotba01_cliente>()
                .Property(e => e.dominio_email)
                .IsUnicode(false);

            modelBuilder.Entity<xptotba01_cliente>()
                .Property(e => e.razao_social)
                .IsUnicode(false);

            modelBuilder.Entity<xptotba01_cliente>()
                .Property(e => e.codigo)
                .IsUnicode(false);

            modelBuilder.Entity<xptotba01_cliente>()
                .Property(e => e.canal)
                .IsUnicode(false);

            modelBuilder.Entity<xptotba01_cliente>()
                .HasMany(e => e.xptotba02_analista)
                .WithOptional(e => e.xptotba01_cliente)
                .HasForeignKey(e => e.cliente_hostname);

            modelBuilder.Entity<xptotba01_cliente>()
                .HasMany(e => e.xptotba03_maquina)
                .WithOptional(e => e.xptotba01_cliente)
                .HasForeignKey(e => e.cliente_hostname);

            modelBuilder.Entity<xptotba02_analista>()
                .Property(e => e.nome_usuario)
                .IsUnicode(false);

            modelBuilder.Entity<xptotba02_analista>()
                .Property(e => e.senha)
                .IsUnicode(false);

            modelBuilder.Entity<xptotba02_analista>()
                .Property(e => e.cliente_hostname)
                .IsUnicode(false);

            modelBuilder.Entity<xptotba03_maquina>()
                .Property(e => e.serial)
                .IsUnicode(false);

            modelBuilder.Entity<xptotba03_maquina>()
                .Property(e => e.cliente_hostname)
                .IsUnicode(false);

            modelBuilder.Entity<xptotba03_maquina>()
                .HasMany(e => e.xptotba06_hardware)
                .WithOptional(e => e.xptotba03_maquina)
                .HasForeignKey(e => e.maquina_serial);

            modelBuilder.Entity<xptotba03_maquina>()
                .HasMany(e => e.xptotba05_hd)
                .WithOptional(e => e.xptotba03_maquina)
                .HasForeignKey(e => e.maquina_serial);

            modelBuilder.Entity<xptotba03_maquina>()
                .HasMany(e => e.xptotba04_usuariomaquina)
                .WithOptional(e => e.xptotba03_maquina)
                .HasForeignKey(e => e.maquina_serial);

            modelBuilder.Entity<xptotba03_maquina>()
                .HasMany(e => e.xptotba07_monitoramento)
                .WithOptional(e => e.xptotba03_maquina)
                .HasForeignKey(e => e.maquina_serial);

            modelBuilder.Entity<xptotba04_usuariomaquina>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<xptotba04_usuariomaquina>()
                .Property(e => e.maquina_serial)
                .IsUnicode(false);

            modelBuilder.Entity<xptotba05_hd>()
                .Property(e => e.descricao)
                .IsUnicode(false);

            modelBuilder.Entity<xptotba05_hd>()
                .Property(e => e.maquina_serial)
                .IsUnicode(false);

            modelBuilder.Entity<xptotba06_hardware>()
                .Property(e => e.arquiteura_sistema_operacional)
                .IsUnicode(false);

            modelBuilder.Entity<xptotba06_hardware>()
                .Property(e => e.codigo_processador)
                .IsUnicode(false);

            modelBuilder.Entity<xptotba06_hardware>()
                .Property(e => e.data_fabricacao)
                .IsUnicode(false);

            modelBuilder.Entity<xptotba06_hardware>()
                .Property(e => e.descricao_fabricante)
                .IsUnicode(false);

            modelBuilder.Entity<xptotba06_hardware>()
                .Property(e => e.descricao_processador)
                .IsUnicode(false);

            modelBuilder.Entity<xptotba06_hardware>()
                .Property(e => e.descricao_sistema_operacional)
                .IsUnicode(false);

            modelBuilder.Entity<xptotba06_hardware>()
                .Property(e => e.espaco_total_ram)
                .IsUnicode(false);

            modelBuilder.Entity<xptotba06_hardware>()
                .Property(e => e.identificador_processador)
                .IsUnicode(false);

            modelBuilder.Entity<xptotba06_hardware>()
                .Property(e => e.modelo_especifico_fabricante)
                .IsUnicode(false);

            modelBuilder.Entity<xptotba06_hardware>()
                .Property(e => e.modelo_fabricante)
                .IsUnicode(false);

            modelBuilder.Entity<xptotba06_hardware>()
                .Property(e => e.quantidade_fisica_processador)
                .IsUnicode(false);

            modelBuilder.Entity<xptotba06_hardware>()
                .Property(e => e.quantidade_logica_processador)
                .IsUnicode(false);

            modelBuilder.Entity<xptotba06_hardware>()
                .Property(e => e.usuario_logado)
                .IsUnicode(false);

            modelBuilder.Entity<xptotba06_hardware>()
                .Property(e => e.maquina_serial)
                .IsUnicode(false);

            modelBuilder.Entity<xptotba07_monitoramento>()
                .Property(e => e.maquina_serial)
                .IsUnicode(false);

            modelBuilder.Entity<xptotba07_monitoramento>()
                .HasMany(e => e.xptotba08_monitoramentohd)
                .WithOptional(e => e.xptotba07_monitoramento)
                .HasForeignKey(e => e.monitoramento_id);

            modelBuilder.Entity<xptotba08_monitoramentohd>()
                .Property(e => e.caminho_absoluto)
                .IsUnicode(false);
        }
    }
}
