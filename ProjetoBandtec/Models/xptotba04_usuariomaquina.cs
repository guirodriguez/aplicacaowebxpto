namespace ProjetoBandtec.Models
{
    using System.ComponentModel.DataAnnotations;

    public partial class xptotba04_usuariomaquina
    {
        public int id { get; set; }

        [StringLength(255)]
        public string email { get; set; }

        [StringLength(255)]
        public string maquina_serial { get; set; }

        public virtual xptotba03_maquina xptotba03_maquina { get; set; }
    }
}
