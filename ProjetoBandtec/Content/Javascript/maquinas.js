﻿$(document).ready(function () {
    $(function () {
        function loopLi() {
            var hostname = $.cookie("login");
            $.getJSON('https://xptows.herokuapp.com/xptoservice/cliente/retornaMaquinas?hostname=' + hostname, function (maquinas) {
                var box = $('#maquinas');
                console.log(maquinas.length);
                for (i = 0; i < maquinas.length; i++) {

                    if (i == 0) {
                        html = '<div class="maquina_esquerda"><img id="maquina_img" src="../Content/images/maquina.png"/>'
                            + '<br/>'
                            + '<label style="font-size:15px; color:orange;" id="lblSistemaOperacional">' + maquinas[i].descricaoSistemaOperacional + '</label>'
                            + '<br/>'
                            + '<br/>'
                            + '<label style="font-size:15px; color:orange;" id="lblNomeUsuarioLogado">' + 'Usuário:' + maquinas[i].usuarioLogado + '</label>'
                            + '<br/>'
                            + '<br/>'
                            + '<center><a href="[link]">' + 'Acessar' + '</a></center>'
                            + '</div>'
                        html = html.replace('[link]', 'http://localhost:50205/XPTO/Maquina?serial=' + maquinas[i].serial);
                        box.append(html);                       
                    } else if (i == 1) {
                        html = '<div class="maquina_meio"><img id="maquina_img" src="../Content/images/maquina.png"/>'
                            + '<br/>'
                            + '<label style="font-size:15px; color:orange;" id="lblSistemaOperacional2">' + maquinas[i].descricaoSistemaOperacional + '</label>'
                            + '<br/>'
                            + '<br/>'
                            + '<label style="font-size:15px; color:orange;" id="lblNomeUsuarioLogado2">' + 'Usuário:' + maquinas[i].usuarioLogado + '</label>'
                            + '<br/>'
                            + '<br/>'
                            + '<center><a href="[link]">' + 'Acessar' + '</a></center>'
                            + '</div>'
                        html = html.replace('[link]', 'http://localhost:50205/XPTO/Maquina?serial=' + maquinas[i].serial);
                        box.append(html);
                    } else {
                        html = '<div class="maquina_direita"><img id="maquina_img" src="../Content/images/maquina.png"/>'
                            + '<br/>'
                            + '<label style="font-size:15px; color:orange;" id="lblSistemaOperacional3">' + maquinas[i].descricaoSistemaOperacional + '</label>'
                            + '<br/>'
                            + '<br/>'
                            + '<label style="font-size:15px; color:orange;" id="lblNomeUsuarioLogado3">' + 'Usuário:' + maquinas[i].usuarioLogado + '</label>'
                            + '<br/>'
                            + '<br/>'
                            + '<center><a href="[link]">' + 'Acessar' + '</a></center>'
                            + '</div>'
                        html = html.replace('[link]', 'http://localhost:50205/XPTO/Maquina?serial=' + maquinas[i].serial);
                        box.append(html);
                    }
                }
            });
            setTimeout(loopLi, 1000000);
        }
        loopLi();
    });
});

