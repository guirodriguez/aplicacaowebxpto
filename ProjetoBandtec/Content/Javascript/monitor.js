$(document).ready(function(){
    var serial = $.cookie("serial");
   $(function(){
		function loopLi() {
            $.getJSON('https://xptows.herokuapp.com/xptoservice/maquina/retornaMonitoramento?serial=' + serial, function(monitoramento) {

                var percentualRam = monitoramento.percentualRam;
                var mensagem = $("#h2_status").text();
                mensagem = mensagem.replace('Normal', 'Aten' + '\u00E7' + '\u00E3' + 'o');
                if (percentualRam > 70) {

                    $("#h2_status").css('color', 'red');
                    $("#h2_status").text(mensagem);
                }
                $("#percentual_cpu").text(monitoramento.temperaturaCpu +"\u00BA"+"C");
				$("#percentual_ram").text(monitoramento.percentualRam + "%");
				$("#percentual_hd").text(monitoramento.listaMonitoramentoHd[0].percentualUtilizado + "%");
            });
                    setTimeout(loopLi,5000);
		} 

		loopLi();
    });
}); 